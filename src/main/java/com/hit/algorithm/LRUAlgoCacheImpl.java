package com.hit.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LRUAlgoCacheImpl<K, V> extends AbstractAlgoCache<K, V> {
	private Map<K, V> pages;
	private List<K> recentlyUsed;

	public LRUAlgoCacheImpl(int capacity) {
		super(capacity);
		this.pages = new HashMap<>();
		this.recentlyUsed = new ArrayList<>();
	}

	@Override
	public int getCapacity() {
		return super.getCapacity();
	}

	@Override
	public void setCapacity(int capacity) {
		super.setCapacity(capacity);
	}

	/**
	 * Returns the value to which the specified key is mapped, or null if this cache contains no mapping for the key. In addition performs the relevant cache algorithm
	 *
	 * @param key with which the specified value is to be associated
	 * @return the value to which the specified key is mapped, or null if this cache contains no mapping for the key
	 */
	@Override
	public V getElement(K key) {
		if(recentlyUsed.contains(key)) {
			int index = recentlyUsed.indexOf(key);
			recentlyUsed.remove(index);
			recentlyUsed.add(0, key);
			return pages.get(key);
		}

		return null;
	}

	/**
	 * Associates the specified value with the specified key in this cache according to the current algorithm
	 *
	 * @param key   with which the specified value is to be associated
	 * @param value to be associated with the specified key
	 * @return return the value of the element which need to be replaced
	 */
	@Override
	public V putElement(K key, V value) {
		V currentValue = null;

		if(recentlyUsed.size() == getCapacity()){
			currentValue = pages.get(recentlyUsed.get(recentlyUsed.size() - 1));
			removeElement(recentlyUsed.get(recentlyUsed.size() - 1));
		}

		recentlyUsed.add(0, key);
		pages.put(key, value);

		return currentValue;
	}

	/**
	 * Removes the mapping for the specified key from this map if present
	 *
	 * @param key whose mapping is to be removed from the cache according to the current algorithm
	 */
	@Override
	public void removeElement(K key) {
		if(recentlyUsed.contains(key)) {
			int index = recentlyUsed.indexOf(key);
			recentlyUsed.remove(index);
			pages.remove(key);
		}
	}
}
