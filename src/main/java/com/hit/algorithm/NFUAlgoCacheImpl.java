package com.hit.algorithm;

public class NFUAlgoCacheImpl<K, V> extends AbstractAlgoCache<K, V> {

	public NFUAlgoCacheImpl(int capacity) {
		super(capacity);
	}

	@Override
	public int getCapacity() {
		return super.getCapacity();
	}

	@Override
	public void setCapacity(int capacity) {
		super.setCapacity(capacity);
	}

	/**
	 * Returns the value to which the specified key is mapped, or null if this cache contains no mapping for the key. In addition performs the relevant cache algorithm
	 *
	 * @param key with which the specified value is to be associated
	 * @return the value to which the specified key is mapped, or null if this cache contains no mapping for the key
	 */
	@Override
	public V getElement(K key) {
		return super.getElement(key);
	}

	/**
	 * Associates the specified value with the specified key in this cache according to the current algorithm
	 *
	 * @param key   with which the specified value is to be associated
	 * @param value to be associated with the specified key
	 * @return return the value of the element which need to be replaced
	 */
	@Override
	public V putElement(K key, V value) {
		return super.putElement(key, value);
	}

	/**
	 * Removes the mapping for the specified key from this map if present
	 *
	 * @param key whose mapping is to be removed from the cache according to the current algorithm
	 */
	@Override
	public void removeElement(K key) {
		super.removeElement(key);
	}
}
